﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggerFactory loggerFactory = new LoggerFactory();
            ILogger log = loggerFactory.CreateLogger(1);
            log.Log("test");
            AutoParkFactory autoParkFactory = new AutoParkFactory();
            AutoPark autoPark = AutoParkFactory.CreateAutoPark();
            AutoParkPrinter autoParkPrinter = new AutoParkPrinter();
            autoParkPrinter.Print(autoPark);
            PriceCalculator priceCalculator = new PriceCalculator();
            Console.WriteLine(priceCalculator.GetSum(autoPark));
            Console.ReadLine();

        }
    }
}
