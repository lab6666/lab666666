﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
   public class AutoParkPrinter
    {
        public void Print(AutoPark auto)
        {

            Console.WriteLine("Название = " + auto.Name);
            Console.Write("Maшины: ");
            foreach (AbstractCar car in auto.Cars)
            {
                Console.WriteLine(car.Model);
            }
        }

    }
}
