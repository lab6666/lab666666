﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class AutoParkFactory
    {
        public static AutoPark CreateAutoPark()
        {

        AutoPark auto = new AutoPark();
            auto.Name = "AutoPark";
            
            CloseCarBody FordMustang = new CloseCarBody("Ford Mustang", 3000000, 1.539, 238,false, "Машины с закрытым корпусом");
            ArmoredVehicles MercedesBenzGClass = new ArmoredVehicles("Mercedes-Benz G Class", 9000000,2.715,308,false,3, "Бронированные автомобили");
            auto.AddCar(FordMustang);
            auto.AddCar(MercedesBenzGClass);
            return auto;
        }
    }
}
