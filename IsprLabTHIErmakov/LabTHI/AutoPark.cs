﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabTHI
{
    public class AutoPark
    {
        List<AbstractCar> _cars = new List<AbstractCar>();
        string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public List<AbstractCar> Cars
        {
            get { return _cars; }
            set { _cars = value; }
        }
        public int GetSum()
        {
            int sum = 0;
            foreach (AbstractCar car in _cars)
            {
                sum = sum + car.Price;
            }
            return sum;
        }
        public void AddCar(AbstractCar car)
        {
            _cars.Add(car);

        }
        public void DeleteCar(AbstractCar car)
        {
            _cars.Remove(car);

        }
        public void EditCar(AbstractCar car)
        {
            car.Price = int.Parse(Console.ReadLine());
        }
    }
}
