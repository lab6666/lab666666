﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppAutoPark
{
    public partial class ItemAdding : Form
    {
        // строковые константы
        private const string CloseCarBodyConst = "Машины с закрытым корпусом";
        private const string OpenCarBodyConst = "Машины с открытым корпусом";
        private const string ElectricVehicleConst = "Электромобили";
        private const string ArmoredVehicleConst = "Бронированные автомобили";
        private const string ToolTipNumber = "Здесь необходимо ввести число";

        // конструктор для добавления элемента
        public ItemAdding()
        {
            InitializeComponent();

            classCB.Items.AddRange(new String[] {CloseCarBodyConst,
                OpenCarBodyConst, ElectricVehicleConst,ArmoredVehicleConst});

            EnterNumber.SetToolTip(Price, ToolTipNumber);
            EnterNumber.SetToolTip(Power, ToolTipNumber);
            EnterNumber.SetToolTip(weight, ToolTipNumber);
            EnterNumber.SetToolTip(energy, ToolTipNumber);
            EnterNumber.SetToolTip(armor, ToolTipNumber);
            Model.Enabled = false;
            Price.Enabled = false;
            weight.Enabled = false;
            Power.Enabled = false;
            energy.Enabled = false;
            armor.Enabled = false;
            typeroof.Enabled = false;
            yes.Enabled = false;
            no.Enabled = false;
        }
        private AbstractCar _newCar;
        public AbstractCar NewCar
        {
            get { return _newCar; }
            set { _newCar = value; }
        }

        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        // методы, вызываемые при выборе класса объекта
        private void CloseCarBodyChosen()
        {
            Model.Enabled = true;
            Price.Enabled = true;
            weight.Enabled = true;
            Power.Enabled = true;
            energy.Enabled = false;
            armor.Enabled = false;
            typeroof.Enabled = false;
            yes.Enabled = true;
            no.Enabled = true;
            Refresh();
        }
        private void OpenCarBodyChosen()
        {
            Model.Enabled = true;
            Price.Enabled = true;
            weight.Enabled = true;
            Power.Enabled = true;
            energy.Enabled = false;
            armor.Enabled = false;
            typeroof.Enabled = true;
            yes.Enabled = false;
            no.Enabled = false;
            Refresh();
        }
        private void ElectricVehicleChosen()
        {
            Model.Enabled = true;
            Price.Enabled = true;
            weight.Enabled = true;
            Power.Enabled = true;
            energy.Enabled = true;
            armor.Enabled = false;
            typeroof.Enabled = false;
            yes.Enabled = true;
            no.Enabled = true;
            Refresh();
        }
        private void ArmoredVehicleChosen()
        {
            Model.Enabled = true;
            Price.Enabled = true;
            weight.Enabled = true;
            Power.Enabled = true;
            energy.Enabled = false;
            armor.Enabled = true;
            typeroof.Enabled = false;
            yes.Enabled = true;
            no.Enabled = true;
            Refresh();
        }
        // методы, вызываемые при создании объекта определенного класса
        private void CreateCloseCarBody()
        {
            NewCar = new CloseCarBody(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked,CloseCarBodyConst);
 
        }
        private void CreateOpenCarBody()
        {
            NewCar = new OpenCarBody(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), typeroof.Text,OpenCarBodyConst);

        }
        private void CreateArmoredVehicles()
        {
            NewCar = new ArmoredVehicles(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked, Convert.ToInt32(armor.Text),ArmoredVehicleConst);

        }
        private void CreateElectricVehicle()
        {
            NewCar = new ElectricVehicle(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked, Convert.ToInt32(energy.Text),ElectricVehicleConst);

        }
        // метод выбора класса элемента
        private void classCB_SelectedIndexChanged(object sender, EventArgs e)
    
        {
             
            String selected = classCB.SelectedItem.ToString();
            ItemAddingButton.Enabled = true;

            switch (selected)
            {
                case CloseCarBodyConst:
                    {
                        CloseCarBodyChosen();
                        break;
                    }
                case OpenCarBodyConst:
                    {
                        OpenCarBodyChosen();
                        break;
                    }
                case ElectricVehicleConst:
                    {
                        ElectricVehicleChosen();
                        break;
                    }
                case ArmoredVehicleConst:
                    {
                        ArmoredVehicleChosen();
                        break;
                    }
            }
        }


        // метод нажатия кнопки добавления объекта
        private void ItemAddingButton_Click(object sender, EventArgs e)
        {
            try
            {
                String selected = classCB.SelectedItem.ToString();
                switch (selected)
                {
                    case CloseCarBodyConst:
                        {
                            if ((Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                CreateCloseCarBody();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case OpenCarBodyConst:
                        {
                            if ((Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                CreateOpenCarBody();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case ElectricVehicleConst:
                        {
                            if ((Convert.ToInt32(energy.Text) >= 0) && (Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                CreateElectricVehicle();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case ArmoredVehicleConst:
                        {
                            if ((Convert.ToInt32(armor.Text) >= 0) && (Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                CreateArmoredVehicles();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                }

                
                
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Некорректный ввод! " +
                    "Проверьте данные и повторите попытку", "Ошибка");
                Console.WriteLine(ex.Message);
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Некорректный ввод!", "Ошибка");
                Console.WriteLine(ex.Message);
                
            }


        }

        private void CancelAddingButton_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            MessageBox.Show("Вы отменили действие", "Сообщение");
            Close();
        }

        private void no_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void yes_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void typeroof_TextChanged(object sender, EventArgs e)
        {

        }

        private void armor_TextChanged(object sender, EventArgs e)
        {

        }

        private void energy_TextChanged(object sender, EventArgs e)
        {

        }

        private void weight_TextChanged(object sender, EventArgs e)
        {

        }

        private void Power_TextChanged(object sender, EventArgs e)
        {

        }

        private void Price_TextChanged(object sender, EventArgs e)
        {

        }

        private void Model_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
    

