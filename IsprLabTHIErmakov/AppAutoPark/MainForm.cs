﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LabTHI;
using Logic;


namespace AppAutoPark
{
    public partial class MainForm : Form
    {
        // ссылки для дочерних форм
        ItemAdding itemAdding;
        EditAdding editAdding;
        PriceCalculator priceCalculator = new PriceCalculator();
        // создаем автопарк с помощью фабрики
        AutoPark autoPark = AutoParkFactory.CreateAutoPark();
        public void UpdateForm()
        {
            // добавляем обновленные записи
            for (int i = 0; i < autoPark.Cars.Count; i++)
                ItemLB.Items.Add(autoPark.Cars[i].Model +
                    " (" + autoPark.Cars[i].Power + " л.с. " +
                    "Цена: " + autoPark.Cars[i].Price);

            // пересчитываем стоимость автопарка
            titlePrice.Text = "Цена автопарка (в рублях): " +
                Convert.ToString(priceCalculator.GetSum(autoPark));
        }

        public MainForm()
        {
            InitializeComponent();
            UpdateForm();
        }

      private void addButton_Click(object sender, EventArgs e)
        {
            
                // при нажатии на кнопки создается объект формы
                itemAdding = new ItemAdding();
                itemAdding.Show();
                
            
        } 

        private void removeButton_Click(object sender, EventArgs e)
        {
            int index = ItemLB.SelectedIndex;

            // дуракоустойчивость
            if (MessageBox.Show("Вы действительно хотите удалить выделенный элемент списка",
                "Удаление элемента", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                autoPark.Cars.Remove(autoPark.Cars[index]);

                // очищаем лист
                ItemLB.Items.Clear();

                UpdateForm();
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            // при нажатии на кнопки создается объект формы 
            editAdding = new EditAdding(autoPark.Cars[ItemLB.SelectedIndex],
          ItemLB.SelectedIndex);
            editAdding.Show();
            
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            // при каждом возврате на основную форму из дочерних
            // кнопки становятся неактивными
            removeButton.Enabled = false;
            editButton.Enabled = false;

            // обновляем лист после добавления объекта
            if ((itemAdding != null) && (itemAdding.Result == DialogResult.OK)&& (itemAdding.NewCar.Power > 0) && (itemAdding.NewCar.Price > 0)
                 && (itemAdding.NewCar.Weight > 0))
            {
                // добавляем в автопарк объект, созданный в дочерней форме
                autoPark.AddCar(itemAdding.NewCar);

                // меняем значение на null
                // знак того, что это изменение учтено
                itemAdding = null;

                // очищаем лист
                ItemLB.Items.Clear();
                MessageBox.Show("Объект был успешно создан", "Сообщение");

                UpdateForm();

            }
            else if ((itemAdding != null) && (itemAdding.Result == DialogResult.OK))
            {
                MessageBox.Show("Отрицательные значения");
                itemAdding = null;

            }
            if ((editAdding != null) && (editAdding.Result == DialogResult.OK))
            {
                // перезаписываем определенный объект автопарка
                autoPark.Cars[editAdding.SelectedIndex] = editAdding.EditedCar;

                // меняем значение на null
                // знак того, что это изменение учтено
                editAdding = null;

                // очищаем лист
                ItemLB.Items.Clear();
                MessageBox.Show("Изменения сохранены", "Сообщение");
                // обновляем список
                UpdateForm();
            }
 


        }

        private void ItemLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ItemLB.SelectedItem != null)
            {
                removeButton.Enabled = true;
                editButton.Enabled = true;
            }
            else
            {
                removeButton.Enabled = false;
                editButton.Enabled = false;
            }

        }
    }
}
