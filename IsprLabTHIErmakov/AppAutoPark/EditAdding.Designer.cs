﻿namespace AppAutoPark
{
    partial class EditAdding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Price = new System.Windows.Forms.TextBox();
            this.Model = new System.Windows.Forms.TextBox();
            this.Power = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.TextBox();
            this.energy = new System.Windows.Forms.TextBox();
            this.armor = new System.Windows.Forms.TextBox();
            this.typeroof = new System.Windows.Forms.TextBox();
            this.classCB = new System.Windows.Forms.ComboBox();
            this.EditButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.yes = new System.Windows.Forms.RadioButton();
            this.no = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Модель";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Цена";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Мощность";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Вес";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(292, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Энергоемкость";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(292, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Уровень Защиты";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(292, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Наличие перегородки";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(292, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Тип Крыши";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(178, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Класс";
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(146, 211);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(100, 20);
            this.Price.TabIndex = 9;
            // 
            // Model
            // 
            this.Model.Location = new System.Drawing.Point(146, 185);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(100, 20);
            this.Model.TabIndex = 10;
            // 
            // Power
            // 
            this.Power.Location = new System.Drawing.Point(146, 237);
            this.Power.Name = "Power";
            this.Power.Size = new System.Drawing.Size(100, 20);
            this.Power.TabIndex = 11;
            // 
            // weight
            // 
            this.weight.Location = new System.Drawing.Point(146, 263);
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(100, 20);
            this.weight.TabIndex = 12;
            // 
            // energy
            // 
            this.energy.Location = new System.Drawing.Point(421, 192);
            this.energy.Name = "energy";
            this.energy.Size = new System.Drawing.Size(100, 20);
            this.energy.TabIndex = 13;
            // 
            // armor
            // 
            this.armor.Location = new System.Drawing.Point(421, 218);
            this.armor.Name = "armor";
            this.armor.Size = new System.Drawing.Size(100, 20);
            this.armor.TabIndex = 14;
            // 
            // typeroof
            // 
            this.typeroof.Location = new System.Drawing.Point(421, 244);
            this.typeroof.Name = "typeroof";
            this.typeroof.Size = new System.Drawing.Size(100, 20);
            this.typeroof.TabIndex = 15;
            // 
            // classCB
            // 
            this.classCB.FormattingEnabled = true;
            this.classCB.Location = new System.Drawing.Point(226, 123);
            this.classCB.Name = "classCB";
            this.classCB.Size = new System.Drawing.Size(161, 21);
            this.classCB.TabIndex = 17;
            // 
            // EditButton
            // 
            this.EditButton.Location = new System.Drawing.Point(196, 332);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(100, 36);
            this.EditButton.TabIndex = 18;
            this.EditButton.Text = "Изменить";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(385, 332);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(112, 36);
            this.CancelButton.TabIndex = 19;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // yes
            // 
            this.yes.AutoSize = true;
            this.yes.Location = new System.Drawing.Point(421, 275);
            this.yes.Name = "yes";
            this.yes.Size = new System.Drawing.Size(40, 17);
            this.yes.TabIndex = 20;
            this.yes.TabStop = true;
            this.yes.Text = "Да";
            this.yes.UseVisualStyleBackColor = true;
            // 
            // no
            // 
            this.no.AutoSize = true;
            this.no.Location = new System.Drawing.Point(477, 275);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(44, 17);
            this.no.TabIndex = 21;
            this.no.TabStop = true;
            this.no.Text = "Нет";
            this.no.UseVisualStyleBackColor = true;
            // 
            // EditAdding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.no);
            this.Controls.Add(this.yes);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.classCB);
            this.Controls.Add(this.typeroof);
            this.Controls.Add(this.armor);
            this.Controls.Add(this.energy);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.Power);
            this.Controls.Add(this.Model);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EditAdding";
            this.Text = "EditAdding";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.TextBox Model;
        private System.Windows.Forms.TextBox Power;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.TextBox energy;
        private System.Windows.Forms.TextBox armor;
        private System.Windows.Forms.TextBox typeroof;
        private System.Windows.Forms.ComboBox classCB;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.RadioButton yes;
        private System.Windows.Forms.RadioButton no;
    }
}